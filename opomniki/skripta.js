window.addEventListener('load', function() {
	//stran nalozena

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			if(cas <= 0) {
				alert("Opomnik!\n\nZadolžitev " + opomnik.querySelector("div").innerHTML + " je potekla!");
				opomnik.remove();
				opomnik = null;
			} else { 
				casovnik.innerHTML -= 1;	
			}

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
	
	var clickListener = function(){
		
		var name = document.querySelector("#uporabnisko_ime").value;
		document.getElementById("uporabnik").innerHTML = name;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
		
	}
	
	document.getElementById("prijavniGumb").addEventListener("click", clickListener);
	
	var dodajOpomnik = function() {
		
		var cas_opomnika = document.getElementById("cas_opomnika");
		var naziv_opomnika = document.getElementById("naziv_opomnika");
		
		var timeInSeconds = cas_opomnika.value;
		var name = naziv_opomnika.value;
		
		cas_opomnika.value = "";
		naziv_opomnika.value = "";
		
		var toAdd = "<div class='opomnik rob senca'><div class='naziv_opomnika'>" + name + "</div><div class='cas_opomnika'> Opomnik čez <span>" + timeInSeconds + "</span> sekund.</div></div>";
		
		var remindersField = document.getElementById("opomniki");
		remindersField.innerHTML += toAdd;
		
		
	}
	
	document.getElementById("dodajGumb").addEventListener("click", dodajOpomnik);
	
	
	
});
